import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExp {

    public static void main(String[] args) {

        Pattern pattern1 = Pattern.compile("[а-я]+");

        Matcher matcher1 = pattern1.matcher("Регулярные выражения");
        System.out.println(matcher1.find());

        Matcher matcher2 = pattern1.matcher("РЕГУЛЯРНЫЕ ВЫРАЖЕНИЯ 1 2 3 4");
        System.out.println(matcher2.find());

        Pattern pattern2 = Pattern.compile("[а-яА-Я0-9]");

        Matcher matcher3 = pattern2.matcher("РЕГУЛЯРНЫЕ выражения 1 2 3 4 ");
        System.out.println(matcher3.find());
    }
}
