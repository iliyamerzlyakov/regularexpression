import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Metacharacters {

    public static void main(String[] args) {

        Pattern pattern1 = Pattern.compile("^[а-е]");

        Matcher matcher1 = pattern1.matcher("а б в г д е ё ж з");
        System.out.println(matcher1.find());

        Pattern pattern2 = Pattern.compile("[а-е]$");

        Matcher matcher2 = pattern2.matcher("з ж ё е д г в б а");
        System.out.println(matcher2.find());

        Pattern pattern3 = Pattern.compile(".[0-9]");

        Matcher matcher3 = pattern3.matcher("1 2 3 4");
        System.out.println(matcher3.find());

        Pattern pattern4 = Pattern.compile(".[0-9]|.[а-я]");

        Matcher matcher4 = pattern4.matcher("1 2 3 4 абвгд");
        System.out.println(matcher4.find());

        Pattern pattern5 = Pattern.compile("\\d");

        Matcher matcher5 = pattern5.matcher("1 2 3 4 абвгд");
        System.out.println(matcher5.find());

        Pattern pattern6 = Pattern.compile("\\D");

        Matcher matcher6 = pattern6.matcher("1 2 3 4");
        System.out.println(matcher6.find());

        Pattern pattern7 = Pattern.compile("\\s");

        Matcher matcher7 = pattern7.matcher("1 2 3 4 абвгд");
        System.out.println(matcher7.find());

        Pattern pattern8 = Pattern.compile("\\S");

        Matcher matcher8 = pattern8.matcher("1 2 3 4 абвгд");
        System.out.println(matcher8.find());
    }
}
