import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class POSIX {

    public static void main(String[] args) {

        String string = "abcd1234";
        Pattern pattern1 = Pattern.compile("\\p{Digit}");
        Matcher matcher1 = pattern1.matcher(string);
        while (matcher1.find()) {
            System.out.println(matcher1.group());
        }
    }
}
