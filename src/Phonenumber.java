import java.util.regex.Pattern;
import java.util.regex.Matcher;
public class Phonenumber {

    public static void main(String[] args) {

        String phoneNumber = "+79042657965";
        Pattern pattern1 = Pattern.compile("^((\\+?79)([0-9]{9}))");
        Matcher matcher1 = pattern1.matcher(phoneNumber);

        if (matcher1.matches()){
            System.out.println(phoneNumber + " номер телефона правильный");
        }else{
            System.out.println(phoneNumber + " номер телефона неправильный");
        }
    }
}
