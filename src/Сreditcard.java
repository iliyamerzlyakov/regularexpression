import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Сreditcard {

    public static void main(String[] args) {

        String cardNumber = "2234 2345 3456 4567";
        String date = "12/22";
        String CVV = "245";

        Pattern cardNumberPattern = Pattern.compile("([2-6]([0-9]{3}) ?)(([0-9]{4} ?){3})");
        Pattern datePattern = Pattern.compile("(0[1-9]|1[0-2])/([0-9]{2})");
        Pattern CVVPatern = Pattern.compile("[0-9]{3}");

        Matcher cardNumberMatcher = cardNumberPattern.matcher(cardNumber);
        Matcher dateMatcher = datePattern.matcher(date);
        Matcher CVVMatcher = CVVPatern.matcher(CVV);

        if (cardNumberMatcher.matches() && dateMatcher.matches() && CVVMatcher.matches()){
            System.out.println("Данные карты правильные");
        }else{
            System.out.println("Данные карты неправильные");
        }
    }
}
