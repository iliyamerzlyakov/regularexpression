import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Group {

    public static void main(String[] args) {

        Pattern pattern1 = Pattern.compile("(\\d+)");
        Matcher matcher1 = pattern1.matcher("группа 1817, группа 1818, группа 1819");

        while (matcher1.find()){
            System.out.println(matcher1.group(1));
        }

        Pattern pattern2 = Pattern.compile("(\\d+).*\\1");
        Matcher matcher2 = pattern2.matcher("группа 1817, группа 1818, группа 1819");

        while (matcher2.find()){
            System.out.println(matcher2.group(1));
        }

        Pattern pattern3 = Pattern.compile("Экран(?:Компьютера|Телефона)");
        Matcher matcher3 = pattern3.matcher("ЭкранКомпьютера ЭкранТелефона");

        while (matcher3.find()){
            System.out.println(matcher3.group());
        }

        Pattern pattern4 = Pattern.compile("Выпуск (?=7|8)");
        Matcher matcher4 = pattern4.matcher("Выпуск 7 Выпуск 7");

        while (matcher4.find()){
            System.out.println(matcher4.group());
        }

        Pattern pattern5 = Pattern.compile("Выпуск (?=7|8)");
        Matcher matcher5 = pattern5.matcher("Выпуск 9");

        while (matcher5.find()){
            System.out.println(matcher5.group());
        }
        if (!matcher5.find()) System.out.println("Совпадение не найдено");
    }
}
