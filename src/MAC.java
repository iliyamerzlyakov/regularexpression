import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MAC {

    public static void main(String[] args) {

        String MAC = "77:a3:d2:01:ff:63";
        Pattern pattern = Pattern.compile("^(((\\p{XDigit}{2})[:-]){5}\\p{XDigit}{2})$");
        Matcher matcher = pattern.matcher(MAC);

        if (matcher.matches()){
            System.out.println("Адрес правильный");
        }else {
            System.out.println("Адрес неправильный");
        }
    }
}
