import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPv4 {

    public static void main(String[] args) {

        String ipv4 = "1.2.3.4";
        Pattern pattern = Pattern.compile("^(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))$");
        Matcher matcher = pattern.matcher(ipv4);

        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }
}
